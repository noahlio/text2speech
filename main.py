import gtts
import sys
import os

def verifyArgs():
    if len(sys.argv) != 2:
        print("Uso: python3 main.py archivo.txt")
        sys.exit(1)

def verifyFileExists(file):
    if not os.path.isfile(file):
        print(f"El archivo '{file}' no existe.")
        sys.exit(1)

def getDirAndFileName(file):
    dir, fileName = os.path.split(file)
    fileName, _ = os.path.splitext(fileName)
    return dir, fileName

def readFile(file):
    with open(file, "r", encoding="utf-8") as file:
        return file.read()

def createDirIfNotExists(outDir):
    if not os.path.exists(outDir):
        os.makedirs(outDir)

def main():
    verifyArgs()
    file = sys.argv[1]
    verifyFileExists(file)
    dir, fileName = getDirAndFileName(file)
    outDir = dir.replace("text", "speech")
    createDirIfNotExists(outDir)

    tts = gtts.gTTS(readFile(file), lang="es", slow=False)
    fileMP3 = os.path.join(outDir, fileName + ".mp3")
    tts.save(fileMP3)

main()

